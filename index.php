<?php
$includes = "./includes/";
$config = include($includes . 'config.php');
$servers = array('shire', 'binary', 'terra', 'tubes', 'mooo', 'dynamo');
$modules = array ('art', 'header', 'staff', 'official-channels', 'webchat', 'links', 'donate');
$server = htmlspecialchars($_GET["server"]);
$pin = htmlspecialchars($_GET["pin"]);
$type = htmlspecialchars($_GET["type"]);
if ($pin == $config[pin]) {
    if ($type == 'server') {
        if (in_array($server, $servers)) {
             foreach ($modules as $module) {
                if (file_exists ($includes . $module . "/" . $server . '.txt')) {
                    $file = $includes . $module . "/" . $server . '.txt'; //Path to your *.txt file 
                    $contents = file($file); 
                    $string = implode($contents); 
                    echo $string;
                } elseif (file_exists ($includes . $module . "/default.txt")) {
                    $file = $includes . $module . "/default.txt"; //Path to your *.txt file 
                    $contents = file($file); 
                    $string = implode($contents); 
                    echo $string;
                } else {
                    exit(0);
                }
            }
            echo "=|=\n".
                 "=|====\n".
                 "=|=\n";
        } else {
            echo "Your MOTD is in another castle!"; // You done gone fucked up somewhere
        }
    } elseif ($type == 'rules') { // Should probably rewrite this to support server specific rule's
        $file = $includes . "rules.txt"; //Path to your *.txt file 
        $contents = file($file);
        $string = implode($contents);
        echo $string;
    } else {
        echo "Error: Invalid type";
    }
} else { 
    die(); 
}
?>
